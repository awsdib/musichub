import React from 'react';
import './App.css';
import Subform from './Subform/Subform';

function App() {
  return (
    <div className="App">
      <div className="form-container">
        <Subform />
      </div>
    </div>
  );
}

export default App;

import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Form, Button} from 'react-bootstrap';

class CreditData extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      validated: null
    }
  }

  handleSubmit = event => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    this.setState({validated : true})
    this.props.handleNextClick(event)
    event.preventDefault();
    event.stopPropagation();
  };
  
  render () {
    return (
      <Form validated={this.state.validated}  onSubmit={this.handleSubmit}>
        <Form.Group controlId="exampleForm.ControlSelect5">
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Card Number</Form.Label>
            <Form.Control required type="text" placeholder="" value = {this.props.ccNumber} onChange={this.props.ccNumberChanged}/>
          </Form.Group>

          <Form.Group controlId="formBasicEmail">
            <Form.Label>Card expiration date</Form.Label>
            <Form.Control required type="text" placeholder="02/21" value = {this.props.ccExpire} onChange={this.props.ccExpireChanged}/>
          </Form.Group>

          <Form.Group controlId="formBasicEmail">
            <Form.Label>Card security code</Form.Label>
            <Form.Control required type="text" placeholder="123" value = {this.props.ccCode} onChange={this.props.ccCodeChanged}/>
          </Form.Group>
        </Form.Group>

        <div className="control-pane">
          <Button variant="light" onClick={this.props.handlePrevClick}>Previous</Button>
          <Button variant="light" type="submit">Next</Button>
        </div>
      </Form>
    );
  }
}

export default CreditData;
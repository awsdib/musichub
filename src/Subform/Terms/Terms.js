import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Form, InputGroup, FormControl, Button} from 'react-bootstrap';

class Terms extends React.Component {
  constructor(props) {
    super(props)
    this.state = { 
      validated: null
    }
  }

  handleSubmit = event => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    this.setState({validated : true})
    this.props.handleNextClick(event)
    event.preventDefault();
    event.stopPropagation();
  };

  render () {
    return (
      <Form validated={this.state.validated} onSubmit={this.handleSubmit}>
        <InputGroup>
          <FormControl rows="8" as="textarea" aria-label="With textarea">
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
          </FormControl>
        </InputGroup>
        <Form.Check required feedback="You must agree before submitting." type="checkbox" label="I agree to the terms" value={this.props.agree} onChange={this.props.agreeChanged} />

        <div className="control-pane">
          <Button variant="light" onClick={this.props.handlePrevClick}>Previous</Button>
          <Button variant="light" disabled >Next</Button>
          <Button variant="primary" className="confirm" type="submit" >Confirm</Button>
        </div>
      </Form>
    );
  }
}

export default Terms;
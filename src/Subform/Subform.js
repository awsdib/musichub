import React from 'react';
import './Subform.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Nav, Tab, Row, Col} from 'react-bootstrap';
import classnames from 'classnames';
import Subparams from './Subparams/Subparams';
import UserData from './UserData/UserData';
import CreditData from './CreditData/CreditData';
import Terms from './Terms/Terms';
  
class  Subform extends React.Component  {
    constructor(props) {
        super(props);
        this.state = { activeTab: 'first', valid: false, price: 10,
        data: {
            duration : '12 months', usage: '5', upPayment: 'No', 
            firstName : '', lastName : '', email : '', address : '', 
            ccNumber : '', ccExpire : '', ccCode : '', 
            agree: false
        }};
    }

    calculatePrice() {
        let p = 0;
        p = 2 * +this.state.data.usage
        if(this.state.data.upPayment === 'Yes')
            p = p - (p * .1)
        this.setState({price: p})
    }

    setActiveTab = (tab) => {
        this.setState({activeTab:tab});
    }

    handleNextClick = (event) => {
        let active = this.state.activeTab;
        if(active === 'first') {
            this.setActiveTab('second');
        }
        else if(active === 'second') {
            this.setActiveTab('third');
        }
        else if(active === 'third') {
            this.setActiveTab('fourth');
        } else if(active === 'fourth') {
            this.postRequest();
        }
    };

    postRequest = () => {
        fetch('/api/postRequest',{
            method: 'POST',
            body: JSON.stringify({
              data: this.state.data
            }),
            headers: {"Content-Type": "application/json"}
          })
          .then(function(response){
            if(response.status === 200) {
                alert("request sent successfully")
            } else {
                alert("error in validation. Please check the data")
            }
            return response.json()
          }).then(function(body){
            console.log(body);
        });
    }

    handlePrevClick = () => {
        let active = this.state.activeTab;
        if(active === 'second') {
            this.setActiveTab('first');
        }
        else if(active === 'third') {
            this.setActiveTab('second');
        }
        else if(active === 'fourth') {
            this.setActiveTab('third');
        }
    };

    setValid = (v) => {
        this.setState({
            valid: v
        });        
    }

    // -------- *1* Subscription parameters ---------------------
    durationSelect = (e) => {
        this.setState({ 
            data: {
                ...this.state.data, duration: e.target.value 
            }
        });
    }

    usageSelect = (e) => {
        this.setState({
            data: {
                ...this.state.data, usage: e.target.value
            }
        }, () => {
            this.calculatePrice();
        })
    }

    upPaymentChanged = (e) => {
        this.setState({
            data: {
                ...this.state.data, upPayment: e.target.value
            }
        }, () => {
            this.calculatePrice() 
        })
    }

    // -------- *2* User Data ---------------------
    firstNameChanged = (e) => {
        this.setState({
            data: {
                ...this.state.data, firstName: e.target.value
            }
        });
    }

    lastNameChanged = (e) => {
        this.setState({
            data: {
                ...this.state.data, lastName: e.target.value
            }
        });
    }

    emailChanged = (e) => {
        this.setState({
            data: {
                ...this.state.data, email: e.target.value
            }
        });
    }
    
    addressChanged = (e) => {
        this.setState({
            data: {
                ...this.state.data, address: e.target.value
            }
        });
    }

    // -------- *3* Credit Card data ---------------------
    ccNumberChanged = (e) => {
        this.setState({
            data: {
                ...this.state.data, ccNumber: e.target.value
            }
        });
    }

    ccExpireChanged = (e) => {
        this.setState({
            data: {
                ...this.state.data, ccExpire: e.target.value
            }
        });
    }

    ccCodeChanged = (e) => {
        this.setState({
            data: {
                ...this.state.data, ccCode: e.target.value
            }
        });
    }

    // -------- *4* User Data ---------------------
    agreeChanged = (e) => {
        this.setState({
            data: {
                ...this.state.data, agree: e.target.value
            }
        }); 
    }


    render() {
        const key = this.state.activeTab;
        return (
            <div className="subform">
                <div className="intro">
                    Please fill the fields below to complete your subscription
                </div>
                <Tab.Container id="left-tabs-example" defaultActiveKey="first" activeKey={this.state.activeTab}>
                    <Row>
                        <Col sm={3}>
                            <Row>
                                <Nav variant="pills" className="flex-column">
                            <Nav.Item>
                                <Nav.Link eventKey="first" 
                                        className={classnames({ active: key === 'first' })}>
                                    Select Subscription Parameters
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="second"
                                className={classnames({ active: key === 'second' })}>
                                    User data</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="third"
                                className={classnames({ active: key === 'third' })}>
                                    Credit Card data</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="fourth"
                                className={classnames({ active: key === 'fourth' })}>
                                    Confirmation</Nav.Link>
                            </Nav.Item>
                        </Nav>
                            </Row>
                        </Col>
                        <Col sm={9}>
                            <Tab.Content>
                                <Tab.Pane eventKey="first">
                                    <div className="tab-inner">
                                        <Subparams durationSelect={this.durationSelect} 
                                            usageSelect={this.usageSelect}
                                            upPaymentChanged={this.upPaymentChanged}
                                            duration={this.state.data.duration}
                                            usage={this.state.data.usage}
                                            upPayment={this.state.data.upPayment}
                                            setValid={this.setValid}
                                            handleNextClick={this.handleNextClick}
                                            handlePrevClick={this.handlePrevClick}/>
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="second">
                                    <div className="tab-inner">
                                        <UserData firstName = {this.state.data.firstName}
                                            lastName = {this.state.data.lastName}
                                            email = {this.state.data.email}
                                            address = {this.state.data.address} 
                                            firstNameChanged = {this.firstNameChanged} 
                                            lastNameChanged = {this.lastNameChanged} 
                                            emailChanged ={this.emailChanged} 
                                            addressChanged = {this.addressChanged}
                                            setValid={this.setValid}
                                            handleNextClick={this.handleNextClick}
                                            handlePrevClick={this.handlePrevClick}/>
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="third">
                                    <div className="tab-inner">
                                        <CreditData ccNumber={this.state.data.ccNumber} 
                                            ccExpire={this.state.data.ccExpire} 
                                            ccCode={this.state.data.ccCode}
                                            ccNumberChanged={this.ccNumberChanged}
                                            ccExpireChanged={this.ccExpireChanged}
                                            ccCodeChanged={this.ccCodeChanged}
                                            handleNextClick={this.handleNextClick}
                                            handlePrevClick={this.handlePrevClick}/>
                                    </div>
                                </Tab.Pane>
                                <Tab.Pane eventKey="fourth">
                                    <div className="terms-box">
                                        <Terms agree={this.state.data.agree} 
                                            agreeChanged={this.agreeChanged}
                                            handleNextClick={this.handleNextClick}
                                            handlePrevClick={this.handlePrevClick}/>
                                    </div>
                                </Tab.Pane>
                            </Tab.Content>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={3}></Col>
                        <Col sm={9}>
                            <div className="preview-pane">
                                <div>
                                    The selected plan is <strong>{this.state.data.usage}</strong> Gigabytes valid for <strong>{this.state.data.duration}</strong>.
                                    Total monthly price is: <strong>{this.state.price}</strong>$
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Tab.Container>
            </div>
        );
    }
}

export default Subform;

import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Form, Button} from 'react-bootstrap';

const durations = ['3 months', '6 months', '12 months'];
const usages = ['3','5','10','20','30','50'];

class Subparams extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      validated: null
    }
  }
  
  handleSubmit = event => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    this.setState({validated : true})
    this.props.handleNextClick(event)
  };

  render () {
    return (
      <Form noValidate  validated={this.state.validated}>
        <Form.Group controlId="exampleForm.ControlSelect1">
          <Form.Label>Duration</Form.Label>
          <Form.Control required as="select" value={this.props.duration} onChange={this.props.durationSelect}>>
          {
            durations.map(function (d) {
              return <option key={d} value={d}>{d}</option>
            })
          }
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlSelect2">
          <Form.Label>Usage</Form.Label>
          <Form.Control required as="select" value={this.props.usage} onChange={this.props.usageSelect}>>
          {
            usages.map(function (d) {
              return <option key={d} value={d}>{d}</option>
            })
          }
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlSelect2">
          <Form.Label>Upfront Payment</Form.Label>
          <Form.Control required as="select" defaultValue={this.props.upPayment} onChange={this.props.upPaymentChanged}>>
            <option key='Yes' value='Yes'>Yes</option>
            <option key='No' value='No'>No</option>
          </Form.Control>
        </Form.Group>

        <div className="control-pane">
          <Button variant="light" onClick={this.props.handlePrevClick}>Previous</Button>
          <Button variant="light" onClick={this.handleSubmit}>Next</Button>
        </div>
      </Form>
    );
  }
}

export default Subparams;
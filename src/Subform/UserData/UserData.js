import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Form, InputGroup, FormControl, Button} from 'react-bootstrap';

class UserData extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      validated: null
    }
  }

  handleSubmit = event => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    this.setState({validated : true})
    this.props.handleNextClick(event)
    event.preventDefault();
    event.stopPropagation();
  };

  render () {
    
    return (
      <Form validated={this.state.validated}  onSubmit={this.handleSubmit}>
        <Form.Group controlId="exampleForm.ControlSelect3">
          <InputGroup className="mb-3">
              <InputGroup.Prepend>
                  <InputGroup.Text>First and last name</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl required value = {this.props.firstName}  onChange={this.props.firstNameChanged}/>
              <FormControl required value = {this.props.lastName}  onChange={this.props.lastNameChanged}/>
          </InputGroup>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control required type="email" placeholder="Enter email" value = {this.props.email} onChange={this.props.emailChanged}/>
          </Form.Group>

          <Form.Group controlId="formBasicAddress">
            <Form.Label>Street address</Form.Label>
            <Form.Control required type="text" placeholder="Enter address" value = {this.props.address} onChange={this.props.addressChanged}/>
          </Form.Group>

        </Form.Group>
        
        <div className="control-pane">
          <Button variant="light" onClick={this.props.handlePrevClick}>Previous</Button>
          <Button variant="light" type="submit">Next</Button>
        </div>
      </Form>
    );
  }
}

export default UserData;
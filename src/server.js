const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');

app.use(express.static(path.join(__dirname, 'build')));

app.use(bodyParser.json());

app.post('/api/postRequest', (request, response) => {
    
    console.log(request.body);      // your JSON

    let err = '';
    err = isValid(request.body.data);
    if(err != '') {
        response.send('OK');    // echo the result back
    } else {
        response.sendStatus(400).json({
            error: err
        })
    } 
    
});

isValid = (data) => {
    if(data.email.indexOf('@') == -1)
        return 'error in email address'

    if(data.ccNumber.length != 16)
        return 'error in credit card number'

    return '';
}

app.get('*', (req,res) => {
 res.sendFile(path.join(__dirname, 'build/index.html'));
});

const port = process.env.PORT || 8080;

app.listen(port, () => {
 console.log('Listening on port', port);
});